package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/anangnov/test-lemonilo-golang/models"
	"gitlab.com/anangnov/test-lemonilo-golang/models/users"
	"gitlab.com/anangnov/test-lemonilo-golang/utils"
)

type Response struct {
	Username    string `json:"username"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phone_number"`
}

func CreateUser(c *gin.Context) {
	createValidator := users.NewCreateValidation()
	err := c.BindJSON(&createValidator)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusBadRequest,
			"message": "Invalid Form",
		})
		return
	}

	hashPassword, _ := utils.HashPassword(createValidator.Password)
	user := models.Users{Email: createValidator.Email, Address: createValidator.Address, Password: hashPassword}
	models.DB.Create(&user)

	c.JSON(http.StatusOK, gin.H{
		"message": "Created",
		"data":    user,
	})
}

func LoginUser(c *gin.Context) {
	loginValidator := users.NewLoginValidation()
	err := c.BindJSON(&loginValidator)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code":    http.StatusBadRequest,
			"message": "Invalid Form",
		})
		return
	}

	var user models.Users
	sign := jwt.New(jwt.GetSigningMethod("HS256"))
	token, err := sign.SignedString([]byte("secret"))
	if err := models.DB.Where("email = ?", loginValidator.Email).First(&user).Error; err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"code":    http.StatusUnauthorized,
			"message": "Email or password is wrong",
		})
		return
	}

	pass := utils.CheckPasswordHash(loginValidator.Password, user.Password)
	if !pass {
		c.JSON(http.StatusUnauthorized, gin.H{
			"code":    http.StatusUnauthorized,
			"message": "Email or password is wrong",
		})
		return
	}

	jsonData, _ := json.Marshal(user)
	toString := string(jsonData)
	toByte := []byte(toString)
	var response Response
	json.Unmarshal(toByte, &response)

	c.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "Success",
		"token":   token,
		"data":    response,
	})
}
