package models

type Users struct {
	ID       int    `json:"id" gorm:"primary_key"`
	Email    string `json:"email"`
	Address  string `json:"address"`
	Password string `json:"password"`
}
