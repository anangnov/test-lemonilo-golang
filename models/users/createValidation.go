package users

type CreateValidation struct {
	Email    string `json:"email" form:"email" binding:"required"`
	Address  string `json:"address" form:"address" binding:"required"`
	Password string `json:"password" form:"password" binding:"required"`
}

func NewCreateValidation() CreateValidation {
	return CreateValidation{}
}
