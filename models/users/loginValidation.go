package users

type LoginValidation struct {
	Email    string `json:"email" form:"email" binding:"required"`
	Password string `json:"password" form:"password" binding:"required"`
}

func NewLoginValidation() LoginValidation {
	return LoginValidation{}
}
