# test-lemonilo-golang

# FEATURES
1. gin golang
3. gorm golang
2. mySQL


# INITIAL STEP
1. Clone
2. Duplicate .env.example -> .env
3. Modify .env
4. Intall golang modules
```
$ go mod download
```
5. RUN app
```
$ go run .
```
6. Check with open in the browser http://host:port
