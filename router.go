package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/anangnov/test-lemonilo-golang/controllers"
)

func Router() *gin.Engine {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Ping",
		})
	})
	r.POST("/create-user", controllers.CreateUser)
	r.POST("/login", controllers.LoginUser)

	return r
}
